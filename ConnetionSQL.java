import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnetionSQL {

    private  boolean free;
    private  boolean close;

    String url = "jdbc:postgresql://127.0.0.1:5432/postgres" ;
    String user ="postgres";
    String pass ="postgres123";

    Connection con;

    public ConnetionSQL(boolean free ,boolean close) throws SQLException {
        this.free=free;
        this.close=close;
        this.con = DriverManager.getConnection(url,user,pass);
    }

    public ConnetionSQL() {

    }


    boolean getFree(){return free;}
    boolean getClose(){return close;}
    void setFree(boolean value){ this.free = value; }
    void setClose(boolean value){this.close=value;}


}

